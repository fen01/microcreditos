import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PruebaComponent} from 'src/app/components/prueba/prueba.component'

const routes: Routes = [
  {path: '*', component: PruebaComponent},
  {path: 'prueba', component: PruebaComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
