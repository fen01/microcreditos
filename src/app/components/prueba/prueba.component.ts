import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {
  private _jsonURL = 'assets/data.json';
  data:any
  constructor(	private http: HttpClient) {
    
   }

  ngOnInit() {
    this.CargarMostrar()    
  }
  
  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
    
    }
  public CargarMostrar(){
    //cargo el json
    this.getJSON().subscribe(info=>[
    this.data=info,
    //muestro los email
    console.log("Email:"),
    Object.keys(this.data).forEach(posicion => console.log(this.data[posicion].email))
    ]
    )
   
  }
}
